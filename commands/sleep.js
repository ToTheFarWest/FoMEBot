const Command = require('@/structures/Command.js')

class Sleep extends Command {
    constructor (client) {
        super(client, {
            name: "sleep",
            description: "Puts the bot to sleep, or if it is asleep, wakes it up",
            permLevel: 5,
            usage: "sleep"
        })
    }

    async run(message, args) {
        if (this.client.isAsleep) {
            this.client.isAsleep = false
            message.channel.send("Waking up!")
        }
        else {
            this.client.isAsleep = true
            message.channel.send("Going to sleep!")
        }
    }
}